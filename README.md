Hello, I'm a Free Culture Freelance Consultant specialized in technology and open source.

Please, contact me at:
- <tel:+34677553492>
- <mailto:sim6@probeta.net>
- https://matrix.to/#/@sim6:matrix.org
- https://nextcloud.com/sharing#sim6@probeta.net@nextcloud.probeta.net
